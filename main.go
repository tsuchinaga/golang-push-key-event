package main

import (
	key "github.com/micmonay/keybd_event"
	"log"
	"time"
)

func main() {
	time.Sleep(3 * time.Second) // メモ帳に切り替えるまでの時間

	kb, err := key.NewKeyBonding()
	if err != nil {
		log.Fatalln(err)
	}

	kb.SetKeys(toKyeEvents("0123456789abcdef", true)...)

	err = kb.Launching()
	if err != nil {
		log.Fatalln(err)
	}
}

func toKyeEvents(str string, enter bool) []int {
	keys := make([]int, 0)

	for _, r := range []rune(str) {
		switch r {
		case '0':
			keys = append(keys, key.VK_0)
		case '1':
			keys = append(keys, key.VK_1)
		case '2':
			keys = append(keys, key.VK_2)
		case '3':
			keys = append(keys, key.VK_3)
		case '4':
			keys = append(keys, key.VK_4)
		case '5':
			keys = append(keys, key.VK_5)
		case '6':
			keys = append(keys, key.VK_6)
		case '7':
			keys = append(keys, key.VK_7)
		case '8':
			keys = append(keys, key.VK_8)
		case '9':
			keys = append(keys, key.VK_9)
		case 'a':
			keys = append(keys, key.VK_A)
		case 'b':
			keys = append(keys, key.VK_B)
		case 'c':
			keys = append(keys, key.VK_C)
		case 'd':
			keys = append(keys, key.VK_D)
		case 'e':
			keys = append(keys, key.VK_E)
		case 'f':
			keys = append(keys, key.VK_F)
		}
	}

	if enter {
		keys = append(keys, key.VK_ENTER)
	}

	return keys
}
